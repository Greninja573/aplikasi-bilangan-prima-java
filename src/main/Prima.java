package main;
import java.util.Scanner;
public class Prima {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int def, awal, akhir;
		
		System.out.println("Aplikasi Bilangan Prima");
		System.out.print("Input angka awal: ");
		awal=scan.nextInt();
		System.out.print("Input angka akhir: ");
		akhir=scan.nextInt();
		System.out.println("Loading...");
		for(int i=awal; i<=akhir; i++) {
			def=0;
			for(int j=1;j<=i;j++) {
				if (i%j==0) {
					def=def+1;
				}
			}
			if(def==2) {
				System.out.print(i+" ");
			}
		}

	}

}
