package main;
import java.util.Scanner;
public class PrimaAtauBukan {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int def, check=0;
		
		System.out.println("Aplikasi Pengecekan Bilangan Prima atau Bukan");
		System.out.print("Masukkan Angka: ");
		def=scan.nextInt();
		System.out.println("Loading...");
		
		for (int i=2; i<=def; i++) {
			if(def%i==0) {
				check++;
			}
		}
		if (check ==1) {
			System.out.println(def+" adalah bilangan prima");
		}else {
			System.out.println(def+" bukan bilangan prima");
		}

	}

}
