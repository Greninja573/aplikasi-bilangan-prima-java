# Aplikasi Bilangan Prima

Aplikasi ini akan menghasilkan angka bilangan prima sesuai angka minimal dan maksimal yang di input

## Cara Menggunakan

Cukup Input angka minimal dan maksimal, aplikasi akan mengeluarkan output bilangan prima

# Aplikasi Validasi Bilangan Prima

Aplikasi ini akan menvalidasi apakah angka yang di input bilangan prima atau bukan

## Cara Menggunakan

Cukup input angka yang di inginkan, aplikasi akan memeriksa apakah angka tersebut bilangan prima atau bukan

# Development

Aplikasi ini dikembangkan dengan bahasa pemograman Java dengan IDE Eclipse

Made by Rifqi Fadhilah ([Gitlab](https://www.gitlab.com/Greninja573))